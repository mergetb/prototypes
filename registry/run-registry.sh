#!/bin/bash

docker run -d \
  --name registry \
  -v "$(pwd)"/certs/:/certs \
  -e REGISTRY_HTTP_ADDR=0.0.0.0:443 \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/reg.pem \
  -e REGISTRY_HTTP_TLS_KEY=/certs/reg-key.pem \
  -p 4433:443 \
  registry:2

