# Prototypes

This repository contains MergeTB system prototypes. There are three prototypes
at this time.

- [mini](mini): A MergeTB portal that runs on a minimal underlying virtual
  infrastructure. A 3 node Kubernetes cluster with a single proxy. This system
  is useful for teseting core Portal functionality.

- [mighty](mighty):  A MergeTB portal that runs on an underlying virtual
  infrastructure capable of testing fault tolerance properties. Dual front door
  proxies, and 3 master k8s system + 3 worker k8s system.

- [integrated](integrated): This is a mini merge portal combined with a Merge
  site that runs the [Cogs](https://gitlab.com/mergetb/tech/cogs) system for 
  end-to-end testing.

All prototypes use [Raven](https://gitlab.com/rygoo/raven) for building virtual
testbed topologies.

## Docs

Documentation on MergeTB site and Portal design and implementation can be found
here

- **site**: https://mergetb.org/docs/facility/
- **portal**: https://mergetb.org/docs/portal/
