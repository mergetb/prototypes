let name = "merge_"+Math.random().toString().substr(-6);

function v2v(a, ai, b, bi, props={}) {
  lnk = Link(a, ai, b, bi, props)
  lnk.v2v = true
  return lnk
}

topo = {
  name: name,
  nodes: [ 
    // ===== portal =======
    deb('px', 2, 2),   // proxy
    deb('ma0', 2, 4),  // master
    deb('ma1', 2, 4),
    deb('ma2', 2, 4),
    deb('sw', 8, 8),   // service-worker
    deb('xw', 8, 8),   // xdc-worker
    deb('ops', 2, 2),  // operations
    stor('st0'),       // storage
    stor('st1'),
    stor('st2'),

    // ==== site ==========
    bullseye('cmdr'),

    // stor nodes
    bullseye('stor0', true), bullseye('stor1', true), 
    bullseye('stor2', true), bullseye('stor3', true),

    // network emulation nodes
    moa('emu0'), moa('emu1'),

    // testbed nodes
    node('n0'), node('n1'), node('n2'), node('n3'),
    node('n4'), node('n5'), node('n6'), node('n7'),
  ],
  switches: [ 
    // portal
    cx('psw'),

    // site
    cx('ileaf0'), cx('ileaf1'),
    cx('ispine'),
    cx('xleaf0'), cx('xleaf1'), cx('xleaf2'), cx('xleaf3'),
    cx('xspine'),
    cx('gw')
  ],
  links: [
    // portal
    Link('px', 1, 'psw', 1),
    Link('ma0', 1, 'psw', 2),
    Link('ma1', 1, 'psw', 3),
    Link('ma2', 1, 'psw', 4),
    Link('sw', 1, 'psw', 5),
    Link('xw', 1, 'psw', 6),
    Link('ops', 1, 'psw', 7),
    Link('st0', 1, 'psw', 8),
    Link('st1', 1, 'psw', 9),
    Link('st2', 1, 'psw', 10),

    // site
    v2v('cmdr', 1, 'gw', 1),

    v2v('n0', 1, 'ileaf0', 1, {mac: {n0: '04:70:00:01:70:1A'}, "boot": 2}),
    v2v('n0', 2, 'xleaf0', 1, {mac: {n0: '04:70:11:01:70:1A'}}),
    v2v('n0', 3, 'xleaf1', 1, {mac: {n0: '04:07:11:01:70:2A'}}),

    v2v('n1', 1, 'ileaf0', 2, {mac: {n1: '04:70:00:01:70:1B'}, "boot": 2}),
    v2v('n1', 2, 'xleaf0', 2, {mac: {n1: '04:70:11:01:70:1B'}}),
    v2v('n1', 3, 'xleaf1', 2, {mac: {n1: '04:70:11:01:70:2B'}}),

    v2v('n2', 1, 'ileaf0', 3, {mac: {n2: '04:70:00:01:70:1C'}, "boot": 2}),
    v2v('n2', 2, 'xleaf0', 3, {mac: {n2: '04:70:11:01:70:1C'}}),
    v2v('n2', 3, 'xleaf1', 3, {mac: {n2: '04:70:11:01:70:2C'}}),

    v2v('n3', 1, 'ileaf0', 4, {mac: {n3: '04:70:00:01:70:1D'}, "boot": 2}),
    v2v('n3', 2, 'xleaf0', 4, {mac: {n3: '04:70:11:01:70:1D'}}),
    v2v('n3', 3, 'xleaf1', 4, {mac: {n3: '04:70:11:01:70:2D'}}),

    v2v('n4', 1, 'ileaf1', 1, {mac: {n4: '04:70:00:01:70:1E'}, "boot": 2}),
    v2v('n4', 2, 'xleaf2', 1, {mac: {n4: '04:70:11:01:70:1E'}}),
    v2v('n4', 3, 'xleaf3', 1, {mac: {n4: '04:70:11:01:70:2E'}}),

    v2v('n5', 1, 'ileaf1', 2, {mac: {n5: '04:70:00:01:70:1F'}, "boot": 2}),
    v2v('n5', 2, 'xleaf2', 2, {mac: {n5: '04:70:11:01:70:1F'}}),
    v2v('n5', 3, 'xleaf3', 2, {mac: {n5: '04:70:11:01:70:2F'}}),

    v2v('n6', 1, 'ileaf1', 3, {mac: {n6: '04:70:00:01:70:11'}, "boot": 2}),
    v2v('n6', 2, 'xleaf2', 3, {mac: {n6: '04:70:11:01:70:11'}}),
    v2v('n6', 3, 'xleaf3', 3, {mac: {n6: '04:70:11:01:70:21'}}),

    v2v('n7', 1, 'ileaf1', 4, {mac: {n7: '04:70:00:01:70:12'}, "boot": 2}),
    v2v('n7', 2, 'xleaf2', 4, {mac: {n7: '04:70:11:01:70:12'}}),
    v2v('n7', 3, 'xleaf3', 4, {mac: {n7: '04:70:11:01:70:22'}}),

    v2v('ileaf0', 5, 'ispine', 1),
    v2v('ileaf1', 5, 'ispine', 2),

    v2v('xleaf0', 5, 'xspine', 1),
    v2v('xleaf1', 5, 'xspine', 2),
    v2v('xleaf2', 5, 'xspine', 3),
    v2v('xleaf3', 5, 'xspine', 4),

    v2v('ispine', 3, 'gw', 2),

    v2v('stor0', 1, 'ispine', 4),
    v2v('stor1', 1, 'ispine', 5),
    v2v('stor2', 1, 'ispine', 6),
    v2v('stor3', 1, 'ispine', 7),

    v2v('emu0', 1, 'xspine', 5),
    v2v('emu1', 1, 'xspine', 6),
  ]
};

// portal .....................................................................

function deb(name, cores, mem) {
    return {
        name: name,
        image: 'debian-bullseye',
        cpu: { cores: cores },
        memory: { capacity: GB(mem) },
    }
}

function stor(name) {
    return {
        name: name,
        image: 'debian-bullseye',
        cpu: { cores: 4 },
        memory: { capacity: GB(4) },
        disks: [
            { size: "5G", dev: 'vdb', bus: 'virtio' },
            { size: "5G", dev: 'vdc', bus: 'virtio' },
            { size: "5G", dev: 'vdd', bus: 'virtio' }
        ],
    }
}

// site .......................................................................

function node(name) {
  return {
    name: name,
    image: 'netboot',
    os: 'netboot',
    cpu: { cores: 4 },
    defaultnic: 'e1000',
    memory: { capacity: GB(4) },
    dedicateddisk: true,
    diskcache: "unsafe",
    defaultdisktype: { dev: 'sda', bus: 'sata' },
    firmware: env.PWD+'/site/firmware/OVMF.fd',
    video: 'vga'
  };
}


function bullseye(name, ddisk = false) {
  return {
    name: name,
    image: 'debian-bullseye',
    memory: { capacity: GB(4) },
    cpu: { cores: 4 },
    dedicateddisk: ddisk,
    diskcache: "unsafe",
  };
}

function moa(name, ddisk = false) {
  return {
    name: name,
    image: 'debian-kass',
    memory: { capacity: GB(16) },
    cpu: { cores: 4, passthru: true },
    dedicateddisk: ddisk,
    diskcache: "unsafe"
  };
}

// common .....................................................................

function cx(name) {
  return {
    name: name,
    image: 'cumulusvx-4.1',
    cpu: { cores: 2 },
    memory: { capacity: GB(2) }
  };
}
