#!/usr/bin/env bash

if [[ $# -ne 3 ]]; then
    echo Usage: $0 project experiment xdcname
    exit 1
fi

sudo ansible-playbook -i .rvn/ansible-hosts --extra-vars "xdc=$3.$2.$1" xdc-setup.yml
