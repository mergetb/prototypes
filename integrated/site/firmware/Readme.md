# Included Firmware

This folder contains machine firmware that is used in the integrated prototype.

- **OVMF.fd** - This OVMF firmware is from the Debian Buster OVMF package.
  Version 0~20181115.85588389-3.

In the event that new firmware is included, it should be documented in this
Readme.
