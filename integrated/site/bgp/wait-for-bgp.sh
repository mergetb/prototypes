#!/bin/bash

state=`gobgp n -j | jq '.[0].state.session_state'`

i=0
while [[ $state != 6 ]]; do
  echo "[$i] waiting for bgp peer state=6 ($state)"
  state=`gobgp n -j | jq '.[0].state.session_state'`
  i=$((i+1))
  if [[ $((i%60)) == 0 ]]; then
    sudo service gobgpd restart
  fi
  sleep 1
done

