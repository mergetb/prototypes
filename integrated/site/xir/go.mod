module tbxir

go 1.12

require (
	github.com/fatih/color v1.9.0 // indirect
	github.com/fogleman/gg v1.3.0 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/kr/pty v1.1.4 // indirect
	github.com/sirupsen/logrus v1.4.1 // indirect
	github.com/stretchr/objx v0.2.0 // indirect
	gitlab.com/mergetb/xir v0.2.12-0.20200329004600-c0c0205b594d
	golang.org/x/crypto v0.0.0-20190513172903-22d7a77e9e5f // indirect
	golang.org/x/image v0.0.0-20190516052701-61b8692d9a5c // indirect
	golang.org/x/net v0.0.0-20190514140710-3ec191127204 // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190517183331-d88f79806bbd // indirect
)

// replace gitlab.com/mergetb/xir => /space/img/xir
