#!/bin/bash

set -e

if [[ $UID -ne 0 ]]; then
  echo "must be root"
  exit 1
fi

rm -rf /tmp/portal-keygen


CONFIG=${CONFIG:-config.yml}
source build-helpers.sh
if [[ ! -f "$CONFIG" ]]; then
	generate_config "$CONFIG"
	editor $CONFIG || true
fi
eval $(parse_yaml $CONFIG)
check_sanity

if [[ -z "$(ls -A tb-deploy)" || -z "$(ls -A portal-deploy)" ]]; then
    stage "initializing tb/portal-deploy submodules"
    git submodule update --init
fi

stage "destroying any exiting topologies"
rvn destroy

stage "building topology"
rvn build

stage "deploying topology"
rvn deploy

stage "waiting for topology to come up"
rvn pingwait \
    px ma0 ma1 ma2 sw xw ops st0 st1 st2 psw \
    cmdr stor0 stor1 stor2 stor3 gw ispine ileaf0 ileaf1 xspine xleaf0 xleaf1 xleaf2 xleaf3

stage "configuring topology"
rvn configure
rvn status

stage "installing ansible roles"
./install-roles.sh

stage "preparing environment"
ansible-playbook prepare.yml

stage "configuring portal"
ansible-playbook \
    -i .rvn/ansible-hosts \
    -i ansible-interpreters \
    --extra-vars @portal/portal-deploy.yml configure-portal.yml

stage "configuring site"
./site-configure.sh
