### Updating Model

Model is stored in `site/xir`.  Main file is tb.go.

```
go build
./tbxir save
```

will generate an updated `spineleaf.mergetb.test.json` for the site to use.  
To load the new xir model of the site, you can run the following modified 
register-site.sh

```
mergetb update site <site> <address> <model.json>
```

You may need to also restart the realization service on the portal (master).

You will also need to copy `/site/xir/spineleaf.mergetb.test.json` into the 
cmdr at /etc/cogs/tb-xir.json. Then restart rex and the driver to pick up the 
new model changes.
