import mergexp as mx
from mergexp.net import capacity, latency
from mergexp.unit import mbps, ms

net = mx.Topology('one-router')

a = net.device('a')
b = net.device('b')
c = net.device('c')

ab = net.connect([a, b], capacity == mbps(10), latency == ms(5))
ab[a].ip.addrs = ['10.0.0.1/24']
ab[b].ip.addrs = ['10.0.0.2/24']

bc = net.connect([b, c], capacity == mbps(100), latency == ms(5))
bc[b].ip.addrs = ['10.0.1.1/24']
bc[c].ip.addrs = ['10.0.1.2/24']

mx.experiment(net)
