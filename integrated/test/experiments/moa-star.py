import mergexp as mx
from mergexp.net import latency
from mergexp.unit import ms

net = mx.Topology('star')

# replace me with your experiment topology
nodes = [net.device(name) for name in ['a', 'b', 'c', 'd']]
router = net.device('router')

for i, n in enumerate(nodes):
    net.connect([router, n], latency == ms(20))
    n.endpoints[0].ip.addrs = ['10.0.%d.10/24'%i]
    router.endpoints[i].ip.addrs = ['10.0.%d.1/24'%i]

mx.experiment(net)
