import mergexp as mx
from mergexp.net import capacity
from mergexp.unit import mbps

net = mx.Topology('dupes')

a1 = net.device('a1')
b = [net.device(name) for name in ['b0', 'b1', 'b2', 'b3']]

ab = net.connect([a1, b[0]], capacity == mbps(100))
ab[a1].ip.addrs   = ['10.0.0.1/24']
ab[b[0]].ip.addrs = ['10.0.0.2/24']

lan = net.connect(b, capacity == mbps(100))
lan[b[0]].ip.addrs = ['10.0.1.1/24']
lan[b[1]].ip.addrs = ['10.0.1.2/24']
lan[b[2]].ip.addrs = ['10.0.1.3/24']
lan[b[3]].ip.addrs = ['10.0.1.4/24']


mx.experiment(net)
