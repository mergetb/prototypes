import mergexp as mx
from mergexp.machine import image

def ubuntu(name, version):
    dev = net.device(name, image == "ubuntu:"+version)
    return dev

def debian(name, version):
    dev = net.device(name, image == "debian:"+version)
    return dev

net = mx.Topology('rainbow')

nodes = [
    ubuntu('a','1804'), 
    ubuntu('b','2004'), 
    debian('c', '10'), 
    debian('d', '11')
]
lan = net.connect(nodes)

for i,e in enumerate(lan.endpoints, 1):
    e.ip.addrs = ['10.0.0.%d/24'%i]
    e.ip.mtu = 9000

mx.experiment(net)
