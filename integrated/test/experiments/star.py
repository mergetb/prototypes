import mergexp as mx

net = mx.Topology('star')

nodes = [net.device(name) for name in ['a', 'b', 'c', 'd']]
router = net.device('router')

for i, n in enumerate(nodes):
    net.connect([router, n])
    n.endpoints[0].ip.addrs = ['10.0.%d.10/24'%i]
    router.endpoints[i].ip.addrs = ['10.0.%d.1/24'%i]

mx.experiment(net)
