import mergexp as mx
from mergexp.machine import image

net = mx.Topology('two')
a = net.device('a', image == 'ubuntu:2004')
b = net.device('b', image == 'ubuntu:1804')
ab = net.connect([a, b])

# access endpoint by link operator
ab[a].ip.addrs = ['10.0.0.1/24']
ab[b].ip.addrs = ['10.0.0.2/24']

# specify the net object as the experiment topology created by this script
mx.experiment(net)

