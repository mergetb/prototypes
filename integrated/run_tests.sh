#!/usr/bin/env bash

set -e

if [[ $# -ne 4 ]]; then
    echo Usage $0 user userpasswd member memberpasswd
    echo
    echo Supported ENV variables: REGISTRY, ORG, TAG for 
    echo determining which containers to run in the portal.
    echo
    exit 1
fi

SRCDIR=$PWD/src

cat << EOF > ./config.yml
portal:
  registry: ${REGISTRY:-quay.io}
  org: ${ORG:-mergetb}
  tag: ${TAG:-ci_testing}
EOF

if [[ -f cogs-pkg.yml ]]; then
    cat cogs-pkg.yml >> ./config.yml
fi

# Swap out the default portal.yml for one that points to the ci_testing
# tagged portal containers.
cp portal/portal-deploy.yml portal/portal-deploy.yml.orig
cp portal/pd-testing.yml portal/portal-deploy.yml

# start the protoype.
./run.sh 

# setup the mergetest container to run the tests.
ansible-playbook -i .rvn/ansible-hosts portal/setup-mergetest.yml

# now run the tests from the mergetest container in the portal.
#
# wanted to use ansible for this, but couldn't work out a clean way to see the test run output
# ansible-playbook -i .rvn/ansible-hosts -e "{ user: $1, passwd: $2, member: $3, mempasswd: $4 }" portal/run-tests.yml
#
# So we do the ssh thing instead.
#
ssh_master="ssh -o StrictHostKeyChecking=no -i /var/rvn/ssh/rvn rvn@$(rvn ip ma0) "

# now start the tests! I don't trust the return value from ssh, so save output and parse it.
RESULTS=$(mktemp /tmp/runtests-out.XXXXXXXX)
trap "rm -f $RESULTS" 0 2 3 15
$ssh_master "kubectl --kubeconfig /var/lib/kubernetes/admin.kubeconfig exec mergetest -- /test/run_tests.sh $1 $2 $3 $4" 2>&1 | tee $RESULTS

# if FAILURES are in the results, exit 1 else 0
grep -q FAILURES $RESULTS && exit 1

exit 0
